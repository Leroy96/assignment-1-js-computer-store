# Computer Store
## Table of Contents

- [Computer Store](#computer-store)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Install](#install)
  - [License](#license)

## Background
Assignment 1 for the JavaScript module.
## Install

`npm install` to install dependencies.

## License

[MIT © Leroy Bemelen.](../LICENSE)
