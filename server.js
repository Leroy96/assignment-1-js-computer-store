// Express
// const express = require('express') // CommonJS
import express from 'express'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'

// Create an instance of Express
const app = express()
const PORT = process.env.PORT || 3000

// Finding filename and create __dirname
const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

// Middleware
app.use(express.static(join(__dirname, 'public')))

// Routes/Endpoint
app.get('/', (req, res) => {
	return res.sendFile(join(__dirname, 'public', 'index.html'))
})

// Start the server
app.listen(PORT, () => console.log(`Server is running on port: ${PORT}`))
