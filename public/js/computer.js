const detailsRowElement = document.getElementById('row-details')

// Fetch data
async function fetchcomputers() {
	try {
		const response = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
		const json = await response.json()
		return json
	} catch (error) {
		console.error('Error', error.message)
	}
}

// Get list of computers
;(async () => {
	const computerSelectElement = document.getElementById('computer-dropdown')
	const computerTitleElement = document.getElementById('computer-title')
	const computerDescriptionElement = document.getElementById('computer-description')
	const computerSpecsElement = document.getElementById('computer-specs')
	const computerPriceElement = document.getElementById('computer-price')
	const computerImageElement = document.getElementById('computer-image')

	// Fetch data
	const computers = await fetchcomputers()

	// Loop over array and fill dropdown
	for (const computer of computers) {
		computerSelectElement.innerHTML += `<option value="${computer.id}">${computer.title}</option>`
	}

	// Functions
	const renderSelectedcomputer = (selectedcomputer) => {
		detailsRowElement.style.display = 'flex'
		computerDescriptionElement.innerText = selectedcomputer.description
		computerTitleElement.innerText = selectedcomputer.title
		computerImageElement.src = 'https://noroff-komputer-store-api.herokuapp.com/' + selectedcomputer.image
		computerSpecsElement.innerHTML = ''
		computerPriceElement.innerText = '€' + selectedcomputer.price + ',-'

		for (const specs of selectedcomputer.specs) {
			computerSpecsElement.innerHTML += `<li>${specs}</li>`
		}
	}

	// computer dropdown
	function dropdownChangeHandler() {
		const computerId = Number(this.value)
		const selectedcomputer = computers.find((data) => data.id === computerId)
		console.log(selectedcomputer)
		renderSelectedcomputer(selectedcomputer)
	}

	// Event listener
	computerSelectElement.addEventListener('change', dropdownChangeHandler)
})()
