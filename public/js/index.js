// Global Variables
const cardElement = document.getElementById('card-container')
const balanceAmountElement = document.getElementById('balance')

const loanElement = document.getElementById('loan')
const loanAmountElement = document.getElementById('loan-amount')
const loanButtonElement = document.getElementById('btn-loan')

const payAmountElement = document.getElementById('pay-amount')
const repayLoanButtonElement = document.getElementById('btn-repay-loan')
const bankButtonElement = document.getElementById('btn-bank')
const workButtonElement = document.getElementById('btn-work')

const computerPriceElement = document.getElementById('computer-price')
const buyComputerElement = document.getElementById('buy-computer')

// Functions
const getLoan = () => {
	if (getLoanBalance() > 0) {
		alert('You cannot get a loan, because you have an outstanding loan.')
	} else {
		let enterAmount = prompt('Please enter the amount:')
		// Only allow numbers in prompt
		while (!/^[0-9]+$/.test(enterAmount)) {
			alert('You did not enter a number.')
			enterAmount = prompt('Please enter the amount:')
		}
		// Convert string value to numbers
		const convertedLoan = Number(enterAmount)

		if (convertedLoan <= getBalance() * 2) {
			if (loanElement.style.display === 'none') {
				loanElement.style.display = 'flex'
				// Loan visible
				repayLoanButtonElement.style.display = 'inline'
			}
			loanAmountElement.innerText = convertedLoan
			balanceAmountElement.innerText = getBalance() + convertedLoan
		} else {
			alert('You cannot get a loan, because your balance is too low.')
		}
	}
}

const goWork = () => {
	payAmountElement.innerText = getPayBalance() + 100
}

const transferToBank = () => {
	const repayment = getPayBalance() * 0.1
	const bankTransfer = getPayBalance() * 0.9
	if (loanElement.style.display !== 'none') {
		if (getLoanBalance() > 0) {
			loanAmountElement.innerText = getLoanBalance() - repayment
			balanceAmountElement.innerText = getBalance() + bankTransfer
		}
		resetPayBalance()
	} else {
		balanceAmountElement.innerText = getBalance() + getPayBalance()
		resetPayBalance()
	}
	checkLoanBalance()
}

const repayLoan = () => {
	if (getPayBalance() > getLoanBalance()) {
		const diffAmount = getPayBalance() - getLoanBalance()
		balanceAmountElement.innerText = getBalance() + diffAmount
	}

	loanAmountElement.innerText = getLoanBalance() - getPayBalance()
	checkLoanBalance()
	resetPayBalance()
}

const buyComputer = () => {
	const computerPrice = Number(computerPriceElement.innerText.replace(/\D/g, ''))
	if (computerPrice > getBalance()) {
		alert('You cannot afford this computer.')
	} else {
		balanceAmountElement.innerText = getBalance() - computerPrice
		alert('You are now the proud owner of a new computer')
	}
}

// Helper functions
const getBalance = () => {
	let balanceAmount = Number(balanceAmountElement.innerText)
	return balanceAmount
}

const getLoanBalance = () => {
	let loanAmount = Number(loanAmountElement.innerText)
	return loanAmount
}

const checkLoanBalance = () => {
	if (loanAmountElement.innerText <= 0) {
		loanElement.style.display = 'none'
	}
}

const getPayBalance = () => {
	let payAmount = Number(payAmountElement.innerText)
	return payAmount
}

const resetPayBalance = () => {
	payAmountElement.innerText = 0
}

// Event listeners
loanButtonElement.addEventListener('click', getLoan)
workButtonElement.addEventListener('click', goWork)
bankButtonElement.addEventListener('click', transferToBank)
repayLoanButtonElement.addEventListener('click', repayLoan)
buyComputerElement.addEventListener('click', buyComputer)
